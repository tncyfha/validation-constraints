package net.tncy.fha.validator;

import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class ISBNValidatorTest {

    private static ISBNValidator validator;
    @BeforeClass
    public static void setUpClass() throws Exception{
        validator = new ISBNValidator();

    }

    @Test
    public void testForbiddenCharacter(){
        assertFalse("Attendu Invalide", validator.isValid("85-359-0277-*", null));

    }

    @Test
    public void testControlKey(){
        assertFalse("Attendu Invalide", validator.isValid("85-359-0277-9", null));

    }

    @Test
    public void testEmpty(){
        assertFalse("Attendu Valide", validator.isValid("", null));

    }

    @Test
    public void testBookland(){
        assertTrue("Attendu Valide", validator.isValid("978-20-20-1", null));

    }

}
