package net.tncy.fha.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ISBNValidator implements ConstraintValidator<ISBN, String> {

    @Override
    public void initialize(ISBN constraintAnnotation){

    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {
        boolean valid = true;
        // Algorithme de validation du numéro ISBN
        return (!bookNumber.equals("") && !bookNumber.contains("*") && (bookNumber.substring(0,3).equals("978") || bookNumber.substring(0,3).equals("979")));
    }


}
